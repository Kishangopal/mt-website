import globalStorage from '../global-storage';
function About(props) {
  return (
    <>
      <section id="aboutSection" style={{ marginTop: 1 }}>
        <div class="container" data-aos="fade-up">
          <div class="section-title">
            <h2>{globalStorage.aboutPage.title}</h2>
            <p>{globalStorage.aboutPage.subTitle}</p>
          </div>
          <div class="row content">
            <div class="col-lg-12">
              <p style={{ textAlign: 'justify' }}>
              <strong>MOVERSTRIP DIGITAL SERVICES LLP </strong> is newly fast growing online platform for Transportation Business. MoversTrip is a name of much efforts and dedication towards facilitate the smoothness for Truck Owners & Transport Business. We are bringing a revolution in the <strong>Transport</strong>  Industry and creating a platform where Truck Owner can get the <strong>Load & Transporter</strong> can get the Truck easily. It is so simple like that.
              </p>
              <p style={{ textAlign: 'justify' }}>
                One of the best thing out of those features is <strong>MTGPS Subscription</strong>. It’s fully loaded feature of security and precise location for your vehicle. You have the full control over your vehicle, no matter it’s far away from you. Sitting at home or may be from your office or roaming anywhere in India, you can check the vehicle Running Status, Vehicle Stop Time Alert, Geo-Fencing, PAN India Network feature, Anti-Theft and loads of available features give you worth to own it.
              </p>
              <p className='animate-charcter' style={{ padding: 20, color: "#fff",fontSize:25,fontFamily:'monospace' }}>Worry not.. MoversTrip is here only for you!</p>
             
              <ul>
              <li>Are you far away from home and your Truck is waiting for business? </li>
              <li>Are you waiting for a Truck for your available Loads?  </li>
              <li>Sitting at home and thinking of getting the business for your Truck! </li>
              <li>Worrying of your Truck location and no control over the Truck because you are not near!</li>
              </ul>
              <p style={{ textAlign: 'justify' }}>
               <strong> MOVERSTRIP DIGITAL SERVICES LLP</strong> gives you a Digital Rate Card option where you can find the Estimated Transportation Cost in just a click. No waiting, just grows your business.
              </p>
              <p style={{ textAlign: 'justify' }}>
                MoversTrip gives you Digital Transport Directory. Where you can find load of information and opportunity to grow your business on your fingertips. We are first one who created such a large scale of information platform in India. It’s definitely fulfil all your business needs.
              </p>
              <p style={{ textAlign: 'justify' }}>MoversTrip Users can get the all details of Transporters in a single click and contact them by call or WhatsApp. Even these business information can be shared to anyone at any time.</p>
              <p style={{ textAlign: 'justify' }}>Our moto is earning happiness for you by giving multiple ways to multiply your business growth. If you stuck anywhere, we be flexible for you to own our services so that you can’t be left behind…we always honour you, your words and your business.</p>
              <p style={{ textAlign: 'justify' }}>We are committed to give you the best and money can’t be a reason to stop us coming closer. So, let us give you a glance of our second subscription TRANSLOAD.</p>
              <p style={{ textAlign: 'justify' }}>Here, just in a few bugs you can avail our most of valuable services & features i.e. Digital Directory, Digital Rate Card, Available Trucks, Available Load etc. Rest of all, you can view all the commercial movement absolutely free. </p>
              <p className='animate-charcter' style={{ padding: 20, color: "#fff",fontSize:25,fontFamily:'monospace' }}>So, Don’t waste you precious time….Go to Google play and download the MoversTrip App immediately.</p>
              <h4>Mission</h4>
              <p style={{ textAlign: 'justify' }}>Creating a resourceful connection between Truck Business and Transportation.</p>
              <h4>Vision</h4>
              <p style={{ textAlign: 'justify' }}>Bringing load of Business all the way to Truck Owner, Goods Owner to Transporters.</p>
              <h4>Value</h4>
              <p style={{ textAlign: 'justify' }}>We are committed for creating genuine opportunity. Transparency, Integrity and Respect forever.</p>
            </div>
          </div>
        </div>
      </section>
    </>
  );
}
export default About;