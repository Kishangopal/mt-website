/* eslint-disable jsx-a11y/iframe-has-title */
import { useState } from "react";
import axios from "axios";
import globalStorage from "../global-storage";
function ContactUs(params) {
  const contactData=[];
  const [name,setName]=useState("");
  const [email,setEmail]=useState("");
  const [subject,setSubject]=useState("");
  const [message,setMessage]=useState("");
  const [contactNumber, setContactNumber] = useState("");
  const [successMsg,setsuccessMsg]=useState(false);
  const handleSubmit = (evt) => {
    evt.preventDefault();
    contactData.push({
      contactName:name,
      emailId:email,
      subject:subject,
      message:message,
      contactNumber:contactNumber,
      status:false
    });
   // setContactValue(contactData);
    console.log(contactData)
    axios.post(`${globalStorage.apiUrl}MtContactUs`, contactData)
    .then((response) => {
      console.log(response);
      setsuccessMsg(true);
      setName("");setEmail("");setSubject("");setMessage("");
    }, (error) => {
      console.log(error);
    });
    //alert(`Json:-${contactValue}`)
}
    return(
        <section id="contact" className="contact">
        <div className="container" data-aos="fade-up">
          <div className="section-title">
            <h2>{globalStorage.contactUs.title}</h2>
            <p>{globalStorage.contactUs.subTitle}</p>\
          </div>
          <div>
          <iframe style={{border:0,width:"100%",height:270}} src={globalStorage.contactUs.mapUrl} width="600" height="450" allowfullscreen="" loading="lazy"></iframe>
          </div>
          <div className="row mt-5">
            <div className="col-lg-4">
              <div className="info">
                <div className="address">
                  <i className="icofont-google-map"></i>
                  <h4>Head Office: </h4>
                  <p>{globalStorage.contactUs.headOfficeAddress}</p>
                </div>
                <div className="address">
                  <i className="icofont-google-map"></i>
                  <h4>Register Office: </h4>
                  <p>{globalStorage.contactUs.registerAddress}</p>
                </div>
                <div className="email">
                  <i className="icofont-envelope"></i>
                  <h4>Email: </h4>
                  <p>{globalStorage.contactUs.email}</p>
                </div>
  
                <div className="phone">
                  <i className="icofont-phone"></i>
                  <h4>Call: </h4>
                  <p>+91 {globalStorage.contactUs.supportNumber}</p>
                </div>
  
              </div>
            </div>
            <div className="col-lg-8 mt-5 mt-lg-0">
            <form onSubmit={handleSubmit}>
              <div className="php-email-form">
                <div className="form-row">
                  <div className="col-md-6 form-group">
                    <input type="text" className="form-control" value={name} onChange={e=>setName(e.target.value)} placeholder="Your Name" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                    <div className="validate"></div>
                  </div>
                  <div className="col-md-6 form-group">
                    <input type="email" className="form-control" value={email} onChange={e=>setEmail(e.target.value)} placeholder="Your Email" data-rule="email" data-msg="Please enter a valid email" />
                 </div>
                 <div className="col-md-6 form-group">
                        <input type="number" className="form-control" value={contactNumber} onChange={e => setContactNumber(e.target.value)} placeholder="Your Contact Number" data-rule="minlen:4" data-msg="Please enter contact number" />
                        <div className="validate"></div>
                      </div>
                      <div className="col-md-6 form-group">
                  <input type="text" className="form-control" value={subject} onChange={e=>setSubject(e.target.value)} placeholder="Subject" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject" />
                  <div className="validate"></div>
                </div>
                </div>
               
                <div className="form-group">
                  <textarea className="form-control" value={message} onChange={e=>setMessage(e.target.value)} rows="5" data-rule="required" data-msg="Please write something for us" placeholder="Message"></textarea>
                  <div className="validate"></div>
                </div>
                <div className="mb-3">
                {successMsg &&
                   <div class="alert alert-success" role="alert">
                   Your Request Send Successfully.
                  </div>
                }
                </div>
                <div className="text-center"><button type="submit">Send Message</button></div>
              </div>
              </form>
            </div>

          </div>
  
        </div>
      </section>      
    );
}
export default ContactUs;