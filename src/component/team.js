
const TeamMember=[
    {
        profileImage:"https://moverstrip-dev.s3.us-east-1.amazonaws.com/system/profile-image.jpg",
        memberName:"Mukesh Sharma",
        jobPost:"Developer",
        facebookUrl:"#",
        twitterUrl:"#",
        instagramUrl:"#",
        linkedinUrl:"#"
    },
    {
        profileImage:"https://moverstrip-dev.s3.us-east-1.amazonaws.com/system/profile-image.jpg",
        memberName:"Mukesh Sharma",
        jobPost:"Developer",
        facebookUrl:"#",
        twitterUrl:"#",
        instagramUrl:"#",
        linkedinUrl:"#"
    },
    {
        profileImage:"https://moverstrip-dev.s3.us-east-1.amazonaws.com/system/profile-image.jpg",
        memberName:"Mukesh Sharma",
        jobPost:"Developer",
        facebookUrl:"#",
        twitterUrl:"#",
        instagramUrl:"#",
        linkedinUrl:"#"
    },
    {
        profileImage:"https://moverstrip-dev.s3.us-east-1.amazonaws.com/system/profile-image.jpg",
        memberName:"Mukesh Sharma",
        jobPost:"Developer",
        facebookUrl:"#",
        twitterUrl:"#",
        instagramUrl:"#",
        linkedinUrl:"#"
    }
]
function Team(params) {
    return (
        <>
            <section id="team" className="team">
                <div className="container" data-aos="fade-up">
                    <div className="section-title">
                        <h2>Team</h2>
                        <p>Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.</p>
                    </div>
                    <div className="row">
                        {TeamMember.map(item =>
                             <div className="col-lg-3 col-md-6 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="100">
                             <div className="member">
                                 <div className="member-img">
                                     <img src={item.profileImage} className="img-fluid" alt="Team Member 1" />
                                         <div className="social">
                                             <a href={item.facebookUrl}><i className="icofont-twitter"></i></a>
                                             <a href={item.twitterUrl}><i className="icofont-facebook"></i></a>
                                             <a href={item.instagramUrl}><i className="icofont-instagram"></i></a>
                                             <a href={item.linkedinUrl}><i className="icofont-linkedin"></i></a>
                                         </div>
               </div>
                                     <div className="member-info">
                                         <h4>{item.memberName}</h4>
                                         <span>{item.jobPost}</span>
                                     </div>
                                 </div>
                             </div>
                        
                            )}
                        </div>
                    </div>
            </section>
        </>
    );
}
export default Team;