import bgImage from '../img/slider-1.png';
import globalStorage from '../global-storage';
function HeroSection(params) {
  return (
    <>
      <section id="hero" class="d-flex align-items-center" style={{ backgroundImage: `url(${bgImage})` }}>
        <div class="container position-relative" data-aos="fade-up" data-aos-delay="100">
          <div class="row justify-content-center">
            <div class="col-xl-7 col-lg-9 text-center">
              <h3 style={{fontWeight:'bold',color:'white'}}>{globalStorage.slider.title}</h3>
              <h5 style={{color:'white'}} >{globalStorage.slider.subTitle}</h5>
            </div>
          </div>  
          <div class="text-center">
            <a href={globalStorage.appurl.moverstripAppUrl} class="btn-get-started scrollto" style={{ marginRight: 5 }}>Download Moverstrip</a>
            <a href={globalStorage.appurl.mtGpsUrl} class="btn-get-started scrollto">Download MTGPS</a>
          </div>
          <div class="row icon-boxes">
            <div class="col-md-12 col-lg-6 align-items-stretch mb-5 mb-lg-0" data-aos="zoom-in" data-aos-delay="200">
              <div class="icon-box">
                <h4 class="title">{globalStorage.App.moverstripApp.title}</h4>
                <p class="description">
                  <ol>
                  {globalStorage.App.moverstripApp.item.map(item =>
                    <li>{item.value}</li>
                  )}
                  </ol>
                </p>
              </div>
            </div>
            <div class="col-md-12 col-lg-6  align-items-stretch mb-5 mb-lg-0" data-aos="zoom-in" data-aos-delay="200">
              <div class="icon-box">
                <h4 class="title">{globalStorage.App.mtGpsApp.title}</h4>
                <p class="description">
                  <ol>
                  {globalStorage.App.mtGpsApp.item.map(item =>
                    <li>{item.value}</li>
                  )}
                  </ol>
                </p>
              </div>
            </div>

          </div>
        </div>
      </section>
    </>
  );
}
export default HeroSection;