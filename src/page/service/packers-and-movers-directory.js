import React, { useState } from "react";
import axios from "axios";
import globalStorage from '../../global-storage';
import AboutLayout from "../../component/about-layout"
function PackersAndMoversDirectory(params) {
  return (
    <>
      <section id="headerImage">
      <img src="https://moverstrip-dev.s3.us-east-1.amazonaws.com/system/websiteStock/header/packersandmovers-header-min.png" style={{width:"100%"}}></img>
      </section>
      <AboutLayout appAbout={globalStorage.appAbout.aboutPackers} classStyle={"about-video"}/>
    </>
  );
}
export default PackersAndMoversDirectory;