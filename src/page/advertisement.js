import React, { useState } from "react";
import axios from "axios";
import globalStorage from '../global-storage';
import EnquiryForm from "../component/enquiry-form"
function Advertisement(params) {
  return (
    <>
      <section id="headerImage">
      <img src="https://moverstrip-dev.s3.us-east-1.amazonaws.com/system/websiteStock/header/Advertisement.jpg" style={{width:"100%"}}></img>
      </section>
      <EnquiryForm type="Advertisement" title={"Request For Advertisement"} subTitle={"If you show your company ads on moverstrip mobile application"}/>
    </>
  );
}
export default Advertisement;